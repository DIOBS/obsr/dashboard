#' @export
to_ton <- function(x) {
  x/1000
}

#' @export
per_capita <- function(x) {
  pop_fortaleza <- 2428708
  x/pop_fortaleza
}

#' @export
br_format <- function (data, precision = 0.1) {
  scales::label_comma(
    accuracy = precision,
    big.mark = ".",
    decimal.mark = ","
  )(data)
}

#' @export
perc_format <- function(perc) {
  ifelse(
    perc < 1,
    "< 1%",
    sprintf("%s%%", br_format(perc)))
}

#' @export
trata_string <- function (word) {
  gsub(" ","", gsub("á","a",
                    gsub("é","e",
                         gsub("í","i",
                              gsub("ó","o",
                                   gsub("ú","u",
                                        gsub("ã","a",
                                             gsub("õ","o",
                                                  gsub("ç","c",
                                                       gsub("â","a",
                                                            gsub("ô","o",
                                                                 gsub("-","", word))))))))))))
}

#' @export
remove_acentos_uppercase <- function (word) {
  gsub("Á","A",
         gsub("É","E",
              gsub("Í","I",
                   gsub("Ó","O",
                        gsub("Ú","U",
                             gsub("Ã","A",
                                  gsub("Õ","O",
                                       gsub("Ç","C",
                                            gsub("Â","A",
                                                 gsub("Ô","O",word))))))))))
}

#' @export
padroniza_ecopontos <- function (ecoponto) {
  gsub("ECOPONTO ", "",
       gsub("ECOPONTO DO ", "",
            gsub(" 1$", "",
                 gsub(" 2$", "",
                      gsub(" I$", "",
                           gsub("MONTE CASTELO", "BAIRRO ELLERY",
                                gsub("CENTRO$", "CENTRO II",
                                     gsub("LESTE$", "LESTE OESTE",
                                          gsub("DESEMBARGADOR GONZAGA$", "CIDADE DOS FUNCIONARIOS",ecoponto)))))))))
}
  
#' @export
day_month_br_format <- function (date) {
  paste0(substr(date, 9, 10),"/",substr(date, 6, 7), "/",substr(date, 1, 4))
}

#' @export
date_br_to_en_format <- function (date_list) {
  lapply(date_list, function(date) {
    paste0(substr(date, 7, 10),"-",substr(date, 4, 5), "-",substr(date, 1, 2))
  })
}


#' @export
month_en_text <- function (date) {
  
  switch (substr(date, 6, 7),
    "01" = "Janeiro",
    "02" = "Fevereiro",
    "03" = "Março",
    "04" = "Abril",
    "05" = "Maio",
    "06" = "Junho",
    "07" = "Julho",
    "08" = "Agosto",
    "09" = "Setembro",
    "10" = "Outubro",
    "11" = "Novembro",
    "12" = "Dezembro"
  )
}

#' @export
month_br_text <- function (date) {
  
  switch (substr(date, 4, 5),
          "01" = "Janeiro",
          "02" = "Fevereiro",
          "03" = "Março",
          "04" = "Abril",
          "05" = "Maio",
          "06" = "Junho",
          "07" = "Julho",
          "08" = "Agosto",
          "09" = "Setembro",
          "10" = "Outubro",
          "11" = "Novembro",
          "12" = "Dezembro"
  )
}

months_list <- c("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro",
                 "Outubro", "Novembro", "Dezembro")

#' @export
months_array_slice <- function (max_month) {
  
  months_list[c(1:max_month)]
}

#' @export
month_br_to_number <- function (month) {
  
  switch (month,
          "Janeiro" = 1,
          "Fevereiro" = 2,
          "Março" = 3,
          "Abril" = 4,
          "Maio" = 5,
          "Junho" = 6,
          "Julho" = 7,
          "Agosto" = 8,
          "Setembro" = 9,
          "Outubro" = 10,
          "Novembro" = 11,
          "Dezembro" = 12
  )
}

#' @export
number_to_month_br <- function (numeric_month) {
  
  months_list[numeric_month]
}

#' @export
padroniza_tipo <- function (lista_tipos) {
  lapply(lista_tipos, function(tipo) {
    switch (tipo,
            "ENTULHO" = "Entulho",
            "VOLUMOSO" = "Volumoso",
            "ENTULHO + VOLUMOSO" = "Total Ent + Vol",
            "PAPEL" = "Papel",
            "PLÁSTICO" = "Plástico",
            "TETRAPAK" = "Tetra Pak",
            "METAL" = "Metal",
            "VIDRO" = "Vidro",
            "ÓLEO" = "Óleo",
            tipo
    )
  })
}

#' @export
padroniza_material <- function (lista_materiais) {
  lapply(lista_materiais, function(material) {
    switch (material,
            "ESPECIAL URBANA" = "Pontos de Lixo",
            "LIXO ESPECIAL URBANA" = "Pontos de Lixo",
            "LIXO ESPECIAL URBANA - MECANIZADA" = "Pontos de Lixo",
            "CAPINA" = "Podas",
            "PODAÇÃO" = "Podas",
            "PODA TRITURADA" = "Podas",
            "ENTULHO" = "Entulho",
            "DOMICILIAR" = "Domiciliar",
            material
    )
  })
}

#' @export
corrige_regional <- function (lista_regionais) {
  lapply(lista_regionais, function(regional) {
    switch (regional,
            "SER I" = "SER 1",
            "SER II" = "SER 2",
            "SER III" = "SER 3",
            "SER IV" = "SER 4",
            "SER V" = "SER 5",
            "SER VI" = "SER 6",
            "SER VII" = "SER 7",
            "SER VIII" = "SER 8",
            "SER IX" = "SER 9",
            "SER X" = "SER 10",
            "SER XI" = "SER 11",
            "SER XII" = "SER 12",
            regional
    )
  })
}

# mypalette <- c("#ffffb2", "#fecc5c", "#F5A109", "#EF771B", "#bd0026")
mypalette <- c("#ffffb2", "#fecc5c", "#fd8d3c", "#f03b20", "#bd0026")

#' @export
add_color <- function (lista_pesos) {
  band_number <- 1
  menor_peso <- lista_pesos[1]
  bandwidth <- (lista_pesos[length(lista_pesos)] - menor_peso) / 4
  result_list <- c()
  
  for (peso in lista_pesos) {
    
    if(peso == 0) {
      
      result_list <- c(result_list, mypalette[1])
      
    } else {
      
      while((peso > menor_peso + band_number * bandwidth) & (band_number < 4)) {
        band_number <- band_number + 1
      }
      
      result_list <- c(result_list, mypalette[band_number+1])
      
    }
    
  }
  
  result_list
}

#' @export
lista_bins <- function (colors_used, bandwidth, menor_peso) {
  
  lapply(colors_used, function(color) {
    switch (color,
            "#ffffb2" = paste0("0,0 - 0,0"),
            "#fecc5c" = paste0(br_format(menor_peso), " - ", br_format(menor_peso + bandwidth)),
            "#fd8d3c" = paste0(br_format(menor_peso + bandwidth), " - ", br_format(menor_peso + 2 * bandwidth)),
            "#f03b20" = paste0(br_format(menor_peso + 2 * bandwidth), " - ", br_format(menor_peso + 3 * bandwidth)),
            "#bd0026" = paste0(br_format(menor_peso + 3 * bandwidth), " - ", br_format(menor_peso + 4 * bandwidth))
    )
  })
}
